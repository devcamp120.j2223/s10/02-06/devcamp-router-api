// Câu lệnh này tương tự câu lệnh import express from 'express'; Dùng để import thư viện express vào project
const express = require("express");

// Khởi tạo app express
const app = express();

// Khai báo cổng của project
const port = 8000;

// Khai báo API dạng get "/" sẽ chạy vào đây
// Callback function: Là một tham số của hàm khác và nó sẽ được thực thi ngay sau khi hàm đấy được gọi
app.get("/", (request, response) => {
    let today = new Date();

    response.status(200).json({
        message: `Xin chào, hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() + 1} năm ${today.getFullYear()}`
    })
})

app.get("/get-method", (request, response) => {
    response.status(200).json({
        info: "GET method"
    })
})

app.post("/post-method", (request, response) => {
    response.status(200).json({
        info: "POST method"
    })
})

app.put("/put-method", (request, response) => {
    response.status(200).json({
        info: "PUT method"
    })
})

app.delete("/delete-method", (request, response) => {
    response.status(200).json({
        info: "DELETE method"
    })
})

// Chạy app express
app.listen(port, () => {
    console.log("App listening on port (Ứng dụng đang chạy trên cổng) " + port);
})